ARG UBUNTU_VERSION=18.04

FROM ubuntu:${UBUNTU_VERSION} AS builder

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    curl \
    build-essential \
    libgtk-3-dev \
    libtiff5-dev \
    libgif-dev \
    libjpeg-dev \
    libpng-dev \
    libxpm-dev \
    libncurses-dev \
    libjansson4 \
    libjansson-dev \
    libgccjit-11-dev \
    libtree-sitter-dev

ARG EMACS_VERSION

RUN curl -LO "https://ftp.gnu.org/gnu/emacs/emacs-${EMACS_VERSION}.tar.gz"

RUN tar xzf emacs-${EMACS_VERSION}.tar.gz
WORKDIR emacs-${EMACS_VERSION}
RUN ./configure  \
    --with-gnutls=no \
    --with-json=yes \
    --with-tree-sitter=yes \
    --with-pgtk=yes \
    --with-native-compilation=yes \
    --prefix=/opt/emacs
RUN make -j$(nproc)
RUN make install

FROM ubuntu:${UBUNTU_VERSION}

ARG EMACS_VERSION

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    libasan8 \
    libgccjit0 \
    libjansson4 \
    libtsan2 \
    libgccjit-11-dev \
    libtree-sitter0

COPY --from=builder /opt/emacs /opt/emacs

COPY env.sh /opt/emacs/.env.sh
COPY adeinit /opt/emacs/.adeinit

VOLUME /opt/emacs
CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
